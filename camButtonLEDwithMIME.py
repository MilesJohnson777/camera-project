'''
I created this project so my kids can send me a photo 
of themselves before they leave the house to get on the bus.
This let's me know they are ready and everything back
home is OK. They can press a button connected to a raspberry pi
which triggers and LED to blink twice quickly then stay on 
while the camera takes a photo. There is also a 5" touchscreen 
attached so they can make sure it looks good.
Every photo taken is sent to my email from a hotmail account I 
setup just for this project. Password for that account is removed.
'''

import os
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import RPi.GPIO as GPIO

# encodes the photos taken into base64 for the attachment and emailed
def send_email():
    fromaddr = "crazyraspi@hotmail.com"
    toaddr = "milesjohnsonwashere@gmail.com"

    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "SUBJECT"

    body = "MESSAGE"
    msg.attach(MIMEText(body, 'plain'))

    filename = "snapshot.jpg"
    attachment = open("/home/pi/Desktop/" + filename, "rb")

    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

    msg.attach(part)

    server = smtplib.SMTP('smtp.live.com', 25)
    server.starttls()
    server.login(fromaddr, "PASSWORD")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()
    print('email sent!')

# Sets up the board pin 12 for the LED and 26 for the momentary button
GPIO.setmode(GPIO.BCM)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# blinks the LED twice, created as a function b/c I originally
# thought I would be using it more than I did...
def LED_blinky():
    GPIO.output(12, True)
    time.sleep(0.2)
    GPIO.output(12, False)
    time.sleep(0.2)
    GPIO.output(12, True)
    time.sleep(0.2)
    GPIO.output(12, False)
    time.sleep(0.2)

# takes a picture at full quality and saves to the desktop
# the photo is always overwritten to conserve space but
# copies are always available in my email
def snip_snap():
    os.system('raspistill -q 100 -o /home/pi/Desktop/snapshot.jpg')
    print('camera done')

# where the main magic happens
# the time is constantly checked to ensure a final
# photo is taken at 8:17 hopefully devoid of any
# little people meaning they are outside waiting for the bus
try:
    while True:
        hour = time.localtime()[3]
        minute = time.localtime()[4]

        #button pressed > light blinks > light stays on
        # > pic taken > light off > email sent
        button_state = GPIO.input(26)
        if button_state == False:
            LED_blinky()
            GPIO.output(12, True)
            snip_snap()
            GPIO.output(12, False)
            send_email()

        # final pic then loop is delayed for 60 seconds
        # to ensure Im not bombarded with 59 (likely less)
        # additional e-mails
        if hour == 8 and minute == 17:
            snip_snap()
            send_email()
            time.sleep(60)

# should my bogus hotmail account get suspended (again...ugh) for
# for suspected spam usage and throws an error that causes the program
# to exit then: <finally: GPIO.cleanup()> is used to return the pins
# back to their original state to ensure that neither the raspberry pi nor
# the hardware connected to them fry before the program actually stops
finally:
    print('GPIO pins reset.')
    GPIO.cleanup()

